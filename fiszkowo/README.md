# Fiszkowo

## App for learning using flashcards,you can create/manage your collection and solve simple quiz to check your memory.

 **Created with:**  
* - Flutter v1.12.13+hotfix.9*  
* - Android Studio 3.6*  


 **Tested on Xiaomi Redmi Note 8 Pro:**  
* - screen size: 6.53 inches*  
* - resolution: 1080 x 2340 pixels, 19.5:9 ratio*  
* - OS: Android 9 Pie, MIUI 11*  
* - RAM: 6GB*  

There might be issues with layout on other devices, i'll try to solve them in the future.