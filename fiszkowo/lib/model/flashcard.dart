class Flashcard {
  String _front;
  String _back;
  int _id;
  int _deckId;

  Flashcard(this._front, this._back, this._deckId);
  Flashcard.withId(this._id, this._front, this._back, this._deckId);

  int get id => _id;
  int get deckId => _deckId;
  String get front => _front;
  String get back => _back;

  set deckId(int id) {
    this._deckId = id;
  }

  set front(String newfront) {
    if (newfront.length <= 255) {
      this._front = newfront;
    }
  }

  set back(String newback) {
    if (newback.length <= 255) {
      this._back = newback;
    }
  }

  //Convert object to map
  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    if (id != null) {
      map['id'] = id;
    }
    map['front'] = front;
    map['back'] = back;
    map['deckId'] = deckId;
    return map;
  }

  //Convert map to object
  Flashcard.toObject(Map<String, dynamic> map) {
    this._id = map['id'];
    this.front = map['front'];
    this.back = map['back'];
    this.deckId = map['deckId'];
  }
}
