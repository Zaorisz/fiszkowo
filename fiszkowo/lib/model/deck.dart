import 'package:fiszkowo/model/flashcard.dart';

class Deck {
  List<Flashcard> _flashcards;
  String _deckname;
  String _description;
  int _id;

  Deck(this._deckname, this._description);
  Deck.withId(this._id, this._deckname, this._description);

  List<Flashcard> get flashcards => _flashcards;
  String get description => _description;
  String get deckname => _deckname;
  int get length => _flashcards.length;
  int get id => _id;

  set flashcards(List<Flashcard> flashcards) {
    this._flashcards = flashcards;
  }

  set deckname(String newTitle) {
    if (newTitle.length <= 255) {
      this._deckname = newTitle;
    }
  }

  set description(String newDescription) {
    if (newDescription.length <= 255) {
      this._description = newDescription;
    }
  }

  //Convert object to map
  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    if (id != null) {
      map['id'] = id;
    }
    map['deckname'] = deckname;
    map['description'] = description;
    return map;
  }

  //Convert map to object
  Deck.toObject(Map<String, dynamic> map) {
    this._id = map['id'];
    this.deckname = map['deckname'];
    this.description = map['description'];
  }
}
