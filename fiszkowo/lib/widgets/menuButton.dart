import 'package:fiszkowo/pages/chosedeck.dart';
import 'package:flutter/material.dart';
import 'package:fiszkowo/pages/decklist.dart';

class MenuButton extends StatelessWidget {
  final String buttonText;
  final int actionIndex;
  MenuButton(this.buttonText, this.actionIndex);

  void handleChoice(BuildContext context, int action) {
    switch (action) {
      case 1:
        {
          Navigator.of(context).push(new MaterialPageRoute(
              builder: (BuildContext context) => ChooseDeck(action)));
        }
        break;
      case 2:
        {
          Navigator.of(context).push(new MaterialPageRoute(
              builder: (BuildContext context) => ChooseDeck(action)));
        }
        break;
      case 3:
        {
          Navigator.of(context).push(new MaterialPageRoute(
              builder: (BuildContext context) => DeckList()));
        }
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(25.0),
      child: RawMaterialButton(
        fillColor: Colors.blue[900],
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(50.0),
        ),
        constraints: BoxConstraints.tight(Size(300, 100)),
        child: Text(
          '$buttonText',
          style: TextStyle(
              fontWeight: FontWeight.bold, fontSize: 50.0, color: Colors.white),
        ),
        onPressed: () {
          handleChoice(context, actionIndex);
        },
      ),
    );
  }
}
