import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:fiszkowo/model/deck.dart';
import 'package:fiszkowo/model/flashcard.dart';

//class to manage database stuff
class DatabaseManager {
  //singletons for keeping only one instance
  static DatabaseManager _databaseManager;
  static Database _database;

  String idCol = 'id';

  String deckTable = 'decks';
  String deckNameCol = 'deckname';
  String deckDescCol = 'description';

  String cardTable = 'flashcards';
  String cardFront = 'front';
  String cardBack = 'back';
  String cardDeckId = 'deckId';

  DatabaseManager._createInstance();

  factory DatabaseManager() {
    //singleton implementation
    if (_databaseManager == null) {
      _databaseManager = DatabaseManager._createInstance();
    }
    return _databaseManager;
  }

  Future<Database> get database async {
    //singleton implementation
    if (_database == null) {
      _database = await initializeDatabase();
    }
    return _database;
  }

  Future<Database> initializeDatabase() async {
    //get directory to store database then open/create
    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path + "fiszkowo.db";
    Database db =
        await openDatabase(path, version: 1, onCreate: _createDatabase);
    return db;
  }

  void _createDatabase(Database db, int newVersion) async {
    await db.execute(
        'CREATE TABLE $deckTable($idCol INTEGER PRIMARY KEY AUTOINCREMENT, $deckNameCol TEXT, $deckDescCol TEXT); ');
    await db.execute(
        'CREATE TABLE $cardTable($idCol INTEGER PRIMARY KEY AUTOINCREMENT, $cardFront TEXT, $cardBack TEXT, $cardDeckId INTEGER);');
  }

  //insert operations
  Future<int> insertDeck(Deck deck) async {
    Database db = await this.database;
    int result = await db.insert(
      deckTable,
      deck.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return result;
  }

  Future<int> insertCard(Flashcard card) async {
    Database db = await this.database;
    int result = await db.insert(cardTable, card.toMap());
    return result;
  }

  //update operations
  Future<int> updateDeck(Deck deck) async {
    Database db = await this.database;
    int result = await db.update(deckTable, deck.toMap(),
        where: '$idCol =?', whereArgs: [deck.id]);
    return result;
  }

  Future<int> updateCard(Flashcard card) async {
    Database db = await this.database;
    int result = await db.update(cardTable, card.toMap(),
        where: '$idCol =?', whereArgs: [card.id]);
    return result;
  }

  //delete operations
  Future<int> deleteDeck(Deck deck) async {
    Database db = await this.database;
    await db.delete(cardTable, where: '$cardDeckId=?', whereArgs: [deck.id]);
    int result =
        await db.delete(deckTable, where: '$idCol=?', whereArgs: [deck.id]);
    return result;
  }

  Future<int> deleteCard(Flashcard card) async {
    Database db = await this.database;
    int result =
        await db.delete(cardTable, where: '$idCol=?', whereArgs: [card.id]);
    return result;
  }

  //getting data from database
  Future<List<Deck>> getDecks() async {
    // get all records from deck table in map format
    Database db = await this.database;
    List<Map<String, dynamic>> map = await db.query(deckTable);
    int count = map.length;
    //prepare variable for deck list
    List<Deck> decks = List<Deck>();
    //iterate over map list and convert to deck object
    for (int i = 0; i < count; i++) {
      decks.add(Deck.toObject(map[i]));
    }
    return decks;
  }

  Future<List<Flashcard>> getCards(int deckId) async {
    // get all records from deck table in map format
    Database db = await this.database;
    List<Map<String, dynamic>> map =
        await db.query(cardTable, where: '$cardDeckId=?', whereArgs: [deckId]);
    int count = map.length;
    //prepare variable for deck list
    List<Flashcard> cards = List<Flashcard>();
    //iterate over map list and convert to deck object
    for (int i = 0; i < count; i++) {
      cards.add(Flashcard.toObject(map[i]));
    }
    return cards;
  }
}
