import 'package:flutter/material.dart';
import 'package:fiszkowo/model/deck.dart';
import 'package:fiszkowo/model/flashcard.dart';
import 'package:fiszkowo/helpers/database_manager.dart';

class LearnPage extends StatefulWidget {
  final Deck deck;
  LearnPage(this.deck);
  @override
  LearnPageState createState() => LearnPageState(deck);
}

class LearnPageState extends State<LearnPage> {
  Deck deck;
  int length = 0;
  int current = 0;
  String frontText = 'brak kart do wyświetlenia';
  String backText = "Naciśnij przycisk aby odsłonić";
  DatabaseManager dbManager = DatabaseManager();
  LearnPageState(this.deck);

  @override
  Widget build(BuildContext context) {
    if (deck.flashcards == null) {
      deck.flashcards = List<Flashcard>();
    }
    updateList();
    frontText = deck.flashcards[current].front;
    return Scaffold(
      backgroundColor: Colors.grey[900],
      appBar: AppBar(
        backgroundColor: Colors.blue[900],
        title: Text(deck.deckname),
        centerTitle: true,
        elevation: 0,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: Container(
              margin: EdgeInsets.all(20.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                color: Colors.blueGrey[200],
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.fromLTRB(20, 20, 30, 20),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        color: Colors.grey),
                    child: Text(
                      "Przód",
                      style: TextStyle(
                        fontSize: 20.0,
                        color: Colors.black,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                        constraints: BoxConstraints.expand(),
                        margin: EdgeInsets.all(20.0),
                        child: Center(
                          child: Text(
                            frontText,
                            style: TextStyle(
                              fontSize: 20.0,
                              color: Colors.black,
                            ),
                          ),
                        )),
                  )
                ],
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Container(
              margin: EdgeInsets.all(20.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                color: Colors.blueGrey[200],
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.fromLTRB(20, 20, 30, 20),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        color: Colors.grey),
                    child: Text(
                      "Tył     ",
                      style: TextStyle(
                        fontSize: 20.0,
                        color: Colors.black,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                        constraints: BoxConstraints.expand(),
                        margin: EdgeInsets.all(20.0),
                        child: Center(
                          child: Text(
                            backText,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 20.0,
                              color: Colors.black,
                            ),
                          ),
                        )),
                  )
                ],
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                    constraints: BoxConstraints.expand(),
                    margin: EdgeInsets.all(20.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      color: Colors.blueGrey[300],
                    ),
                    child: Center(
                      child: Text(
                        'Karta: ' +
                            (current + 1).toString() +
                            '/' +
                            length.toString(),
                        style: TextStyle(
                          fontSize: 20.0,
                          color: Colors.black,
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                      constraints: BoxConstraints.expand(),
                      margin: EdgeInsets.all(20.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        color: Colors.blueGrey,
                      ),
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        color: Colors.blueAccent,
                        child: Text(
                          'Odsłoń',
                          style: TextStyle(
                            fontSize: 20.0,
                            color: Colors.black,
                          ),
                        ),
                        onPressed: () {
                          _revealCard();
                        },
                      )),
                )
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Expanded(
                    flex: 1,
                    child: Container(
                      constraints: BoxConstraints.expand(),
                      margin: EdgeInsets.all(20.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        color: Colors.blueGrey,
                      ),
                      child: RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          color: Colors.greenAccent,
                          child: Text('Poprzednia',
                              style: TextStyle(
                                fontSize: 20.0,
                                color: Colors.black,
                              )),
                          onPressed: () {
                            _previousCard();
                            print('Poprzedni');
                          }),
                    )),
                Expanded(
                    flex: 1,
                    child: Container(
                      constraints: BoxConstraints.expand(),
                      margin: EdgeInsets.all(20.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        color: Colors.blueGrey,
                      ),
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        color: Colors.greenAccent,
                        child: Text('Następna',
                            style: TextStyle(
                              fontSize: 20.0,
                              color: Colors.black,
                            )),
                        onPressed: () {
                          _nextCard();
                          print('Nastepny');
                        },
                      ),
                    ))
              ],
            ),
          )
        ],
      ),
    );
  }

  void _nextCard() {
    if (current + 1 < length) {
      current += 1;
      backText = "Naciśnij przycisk aby odsłonić";
    }
  }

  void _previousCard() {
    if (current - 1 >= 0) {
      current -= 1;
      backText = "Naciśnij przycisk aby odsłonić";
    }
  }

  void _revealCard() {
    setState(() {
      backText = deck.flashcards[current].back;
    });
  }

  Future<void> updateList() async {
    List<Flashcard> cards = await dbManager.getCards(this.deck.id);
    setState(() {
      this.deck.flashcards = cards;
      this.length = cards.length;
    });
  }
}
