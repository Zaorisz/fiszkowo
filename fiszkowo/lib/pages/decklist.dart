import 'dart:async';
import 'package:fiszkowo/helpers/database_manager.dart';
import 'package:flutter/material.dart';
import 'package:fiszkowo/model/deck.dart';
import 'package:fiszkowo/pages/deckdetail.dart';
import 'package:fiszkowo/pages/cardlist.dart';

class DeckList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return DeckListState();
  }
}

class DeckListState extends State<DeckList> {
  List<Deck> deckList;
  int length = 0;
  DatabaseManager dbManager = DatabaseManager();
  @override
  Widget build(BuildContext context) {
//    if (deckList == null) {
//      deckList = List<Deck>();
//    }
//    updateList();
    return Scaffold(
      backgroundColor: Colors.grey[900],
      appBar: AppBar(
        backgroundColor: Colors.blue[900],
        title: Text('Zestawy'),
        centerTitle: true,
        elevation: 1,
      ),
      body: buildDeckList(),
      floatingActionButton: Padding(
        padding: const EdgeInsets.all(20.0),
        child: FloatingActionButton(
          onPressed: () {
            moveToDeckEdit(Deck('', ''), 'Nowy zestaw ');
          },
          child: Icon(Icons.add),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    if (deckList == null) {
      deckList = List<Deck>();
    }
    updateList();
  }

  ListView buildDeckList() {
    return ListView.builder(
      itemCount: length,
      itemBuilder: (BuildContext context, int index) {
        return Card(
          color: Colors.blueGrey,
          child: ListTile(
            onTap: () {
              moveToCardList(deckList[index]);
            },
            leading: CircleAvatar(
                backgroundColor: Colors.white,
                child: Text((index + 1).toString())),
            title: Text(
              deckList[index].deckname,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 24,
                  fontWeight: FontWeight.bold),
            ),
            subtitle: Center(
              child: Text(deckList[index].description,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontStyle: FontStyle.italic)),
            ),
            trailing: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                ConstrainedBox(
                  constraints: BoxConstraints(minHeight: 35, minWidth: 35),
                  child: GestureDetector(
                    child: Icon(Icons.edit, color: Colors.blue),
                    onTap: () {
                      print('edit');
//                      Navigator.of(context).push(new MaterialPageRoute(
//                          builder: (BuildContext context) =>
//                              DeckDetail(deckList[index], 'Edit deck')));
                      moveToDeckEdit(deckList[index], 'Edytuj zestaw');
                    },
                  ),
                ),
                ConstrainedBox(
                  constraints: BoxConstraints(minHeight: 35, minWidth: 35),
                  child: GestureDetector(
                    child: Icon(Icons.delete, color: Colors.redAccent),
                    onTap: () {
                      print('delete');
                      _deleteDeck(context, deckList[index]);
                    },
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void moveToDeckEdit(Deck deck, title) async {
    bool result =
        await Navigator.push(context, MaterialPageRoute(builder: (context) {
      return DeckDetail(deck, title);
    }));

    if (result == true) {
      print(result);
      updateList();
    }
  }

  void moveToCardList(Deck deck) async {
    bool result =
        await Navigator.push(context, MaterialPageRoute(builder: (context) {
      return CardList(deck);
    }));

    if (result == true) {
      print(result);
    }
  }

  void _deleteDeck(BuildContext context, Deck deck) async {
    int result = await dbManager.deleteDeck(deck);
    if (result != 0) {
      _showSnackBar(context, 'Pomyślnie usunięto zestaw');
      updateList();
    }
  }

  void _showSnackBar(BuildContext context, String message) {
    final snackBar = SnackBar(content: Text(message));
    Scaffold.of(context).showSnackBar(snackBar);
  }

  Future<void> updateList() async {
    List<Deck> decks = await dbManager.getDecks();
    setState(() {
      this.deckList = decks;
      this.length = decks.length;
    });
  }
}
