import 'package:flutter/material.dart';
import 'package:fiszkowo/model/deck.dart';
import 'package:fiszkowo/helpers/database_manager.dart';

class DeckDetail extends StatefulWidget {
  final String barTitle;
  final Deck deck;
  DeckDetail(this.deck, this.barTitle);

  @override
  State<StatefulWidget> createState() {
    return DeckDetailState(this.deck, this.barTitle);
  }
}

class DeckDetailState extends State<DeckDetail> {
  String barTitle;
  Deck deck;
  DatabaseManager dbManager = DatabaseManager();
  TextEditingController decknameController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();

  DeckDetailState(this.deck, this.barTitle);

  @override
  Widget build(BuildContext context) {
    decknameController.text = deck.deckname;
    descriptionController.text = deck.description;

    return WillPopScope(
      onWillPop: () {
        moveBack();
      },
      child: Scaffold(
        backgroundColor: Colors.grey[900],
        appBar: AppBar(
          backgroundColor: Colors.blue[900],
          title: Text(barTitle),
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                moveBack();
              }),
        ),
        body: Padding(
          padding: EdgeInsets.only(top: 15.0, left: 10.0, right: 10.0),
          child: ListView(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                child: Container(
                  height: 100,
                  child: TextField(
                    maxLines: null,
                    minLines: null,
                    expands: true,
                    maxLength: 50,
                    controller: decknameController,
                    style: TextStyle(fontSize: 20, color: Colors.white),
                    onChanged: (value) {
                      debugPrint('Something changed in Title Text Field');
                      updateName();
                    },
                    decoration: InputDecoration(
                        labelText: 'Nazwa zestawu',
                        labelStyle:
                            TextStyle(fontSize: 24, color: Colors.white),
                        filled: true,
                        fillColor: Colors.blueGrey,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0))),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                child: Container(
                  height: 200,
                  child: TextField(
                    maxLines: null,
                    minLines: null,
                    expands: true,
                    maxLength: 255,
                    controller: descriptionController,
                    style: TextStyle(fontSize: 20, color: Colors.white),
                    onChanged: (value) {
                      debugPrint('Something changed in Description Text Field');
                      updateDescription();
                    },
                    decoration: InputDecoration(
                        labelText: 'Opis',
                        labelStyle:
                            TextStyle(fontSize: 24, color: Colors.white),
                        filled: true,
                        fillColor: Colors.blueGrey,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0))),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: RaisedButton(
                        color: Colors.blue[900],
                        textColor: Colors.white,
                        child: Text(
                          'Zapisz',
                          textScaleFactor: 1.5,
                        ),
                        onPressed: () {
                          setState(() {
                            debugPrint("Save button clicked");
                            _save();
                          });
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void moveBack() {
    Navigator.pop(context, true);
  }

  void updateName() {
    deck.deckname = decknameController.text;
  }

  void updateDescription() {
    deck.description = descriptionController.text;
  }

  void _save() async {
    moveBack();
    int result;
    if (deck.id == null) {
      result = await dbManager.insertDeck(deck);
    } else {
      result = await dbManager.updateDeck(deck);
    }
    if (result == 0) {
      // Failure
      _showAlertDialog('Status', 'Wystąpił problem');
    }
  }

  void _showAlertDialog(String title, String message) {
    AlertDialog alertDialog = AlertDialog(
      title: Text(title),
      content: Text(message),
    );
    showDialog(context: context, builder: (_) => alertDialog);
  }
}
