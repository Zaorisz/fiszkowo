import 'dart:math';
import 'package:fiszkowo/pages/scorepage.dart';
import 'package:flutter/material.dart';
import 'package:fiszkowo/model/deck.dart';
import 'package:fiszkowo/model/flashcard.dart';
import 'package:fiszkowo/helpers/database_manager.dart';

class QuizPage extends StatefulWidget {
  final Deck deck;
  QuizPage(this.deck);
  @override
  QuizPageState createState() => QuizPageState(deck);
}

class QuizPageState extends State<QuizPage> {
  Deck deck;
  int length = 0;
  int current = 0;
  int correctAnswers = 0;
  String questionText;
  //List<Flashcard> wrongAnswers = List<Flashcard>();
  DatabaseManager dbManager = DatabaseManager();
  QuizPageState(this.deck);
  List<String> possibleAnswers;

  @override
  void initState() {
    super.initState();
    if (deck.flashcards == null) {
      deck.flashcards = List<Flashcard>();
    }
    updateList();
  }

  @override
  Widget build(BuildContext context) {
//    if (this.deck.flashcards == null) {
//      this.deck.flashcards = List<Flashcard>();
//    }
//    updateList();
    possibleAnswers = getPossibleAnswers();
    questionText = this.deck.flashcards[current].front;

    return Scaffold(
      backgroundColor: Colors.grey[900],
      appBar: AppBar(
        backgroundColor: Colors.blue[900],
        title: Text(this.deck.deckname),
        centerTitle: true,
        elevation: 0,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: Container(
              margin: EdgeInsets.all(20.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                color: Colors.blueGrey[200],
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        width: 130,
                        padding: EdgeInsets.fromLTRB(20, 20, 30, 20),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.grey),
                        child: Text(
                          "Co to jest?",
                          style: TextStyle(fontSize: 15.0, color: Colors.black),
                        ),
                      ),
                      SizedBox(
                        width: 100,
                      ),
                      Container(
                        width: 120,
                        padding: EdgeInsets.fromLTRB(20, 20, 30, 20),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.grey),
                        child: Center(
                          child: Text(
                            (current + 1).toString() + '/' + length.toString(),
                            style:
                                TextStyle(fontSize: 15.0, color: Colors.black),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                        constraints: BoxConstraints.expand(),
                        margin: EdgeInsets.all(20.0),
                        child: Center(
                          child: Text(
                            questionText,
                            style:
                                TextStyle(fontSize: 20.0, color: Colors.black),
                          ),
                        )),
                  )
                ],
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.all(15.0),
              child: FlatButton(
                color: Colors.lightBlueAccent,
                child: Text(
                  'A: ' + possibleAnswers[0],
                  style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.black,
                  ),
                ),
                onPressed: () {
                  print('A');
                  handleAnswer(0);
                },
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.all(15.0),
              child: FlatButton(
                color: Colors.lightBlueAccent,
                child: Text(
                  'B: ' + possibleAnswers[1],
                  style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.black,
                  ),
                ),
                onPressed: () {
                  print('B');
                  handleAnswer(1);
                },
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.all(15.0),
              child: FlatButton(
                color: Colors.lightBlueAccent,
                child: Text(
                  'C: ' + possibleAnswers[2],
                  style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.black,
                  ),
                ),
                onPressed: () {
                  print('C');
                  handleAnswer(2);
                },
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.all(15.0),
              child: FlatButton(
                color: Colors.lightBlueAccent,
                child: Text(
                  'D: ' + possibleAnswers[3],
                  style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.black,
                  ),
                ),
                onPressed: () {
                  print('D');
                  handleAnswer(3);
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  List<String> getPossibleAnswers() {
    Random random = Random();
    List<String> answers = List<String>();
    answers.add(this.deck.flashcards[current].back);
    while (answers.length <= 3) {
      int number = random.nextInt(length);
      // if (number != current) {
      answers.add(this.deck.flashcards[number].back);
    }
    //}
    answers.shuffle();
    return answers;
  }

  void handleAnswer(int index) {
    setState(() {
      if (possibleAnswers[index] == deck.flashcards[current].back) {
        //increase correct answers
        correctAnswers++;
      }
//      else {
//        wrongAnswers.add(deck.flashcards[current]);
//      }
      if (isFinished()) {
        //move to score screen
        Navigator.pushReplacement(
            context,
            new MaterialPageRoute(
                builder: (BuildContext context) =>
                    ScorePage(correctAnswers, deck.length)));
      } else {
        //increase flashcard index
        nextFlashcard();
        //get new possible answers
        possibleAnswers = getPossibleAnswers();
      }
    });
  }

  void nextFlashcard() {
    if (current < deck.length - 1) {
      current++;
      questionText = this.deck.flashcards[current].front;
    }
  }

  bool isFinished() {
    if (current >= deck.length - 1) {
      return true;
    } else {
      return false;
    }
  }

  Future<void> updateList() async {
    List<Flashcard> cards = await dbManager.getCards(this.deck.id);
    setState(() {
      this.deck.flashcards = cards;
      this.length = cards.length;
    });
  }
}
