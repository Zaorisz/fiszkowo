import 'dart:async';
import 'package:fiszkowo/helpers/database_manager.dart';
import 'package:fiszkowo/pages/learn.dart';
import 'package:fiszkowo/pages/quizpage.dart';
import 'package:flutter/material.dart';
import 'package:fiszkowo/model/deck.dart';

class ChooseDeck extends StatefulWidget {
  final int action;
  ChooseDeck(this.action);
  @override
  State<StatefulWidget> createState() {
    return ChooseDeckState(action);
  }
}

class ChooseDeckState extends State<ChooseDeck> {
  int action;
  List<Deck> deckList;
  int length = 0;
  DatabaseManager dbManager = DatabaseManager();
  ChooseDeckState(this.action);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[900],
      appBar: AppBar(
        backgroundColor: Colors.blue[900],
        title: Text('Wybierz zestaw'),
        centerTitle: true,
        elevation: 1,
      ),
      body: buildDeckList(),
    );
  }

  @override
  void initState() {
    super.initState();
    if (deckList == null) {
      deckList = List<Deck>();
    }
    updateList();
  }

  ListView buildDeckList() {
    return ListView.builder(
      itemCount: length,
      itemBuilder: (BuildContext context, int index) {
        return Card(
          color: Colors.blueGrey,
          child: ListTile(
            onTap: () {
              if (action == 1) {
//                  Navigator.of(context).push(new MaterialPageRoute(
//                      builder: (BuildContext context) =>
//                          QuizPage(deckList[index])));
                Navigator.pushReplacement(
                    context,
                    new MaterialPageRoute(
                        builder: (BuildContext context) =>
                            QuizPage(deckList[index])));
              } else if (action == 2) {
                Navigator.pushReplacement(
                    context,
                    new MaterialPageRoute(
                        builder: (BuildContext context) =>
                            LearnPage(deckList[index])));
              }
            },
            leading: CircleAvatar(
                backgroundColor: Colors.white,
                child: Text((index + 1).toString())),
            title: Text(
              deckList[index].deckname,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 30,
                  fontWeight: FontWeight.bold),
            ),
          ),
        );
      },
    );
  }

  Future<void> updateList() async {
    List<Deck> decks = await dbManager.getDecks();
    setState(() {
      this.deckList = decks;
      this.length = decks.length;
    });
  }
}
