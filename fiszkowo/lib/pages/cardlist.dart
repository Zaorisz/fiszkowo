import 'dart:async';
import 'package:flutter/material.dart';
import 'package:fiszkowo/model/deck.dart';
import 'package:fiszkowo/model/flashcard.dart';
import 'package:fiszkowo/pages/carddetail.dart';
import 'package:fiszkowo/helpers/database_manager.dart';

class CardList extends StatefulWidget {
  final Deck deck;
  CardList(this.deck);

  @override
  State<StatefulWidget> createState() {
    return CardListState(deck);
  }
}

class CardListState extends State<CardList> {
  Deck deck;
  int length = 0;
  DatabaseManager dbManager = DatabaseManager();
  CardListState(this.deck);
  @override
  Widget build(BuildContext context) {
//    if (deck.flashcards == null) {
//      deck.flashcards = List<Flashcard>();
//    }
//    updateList();
    return Scaffold(
      backgroundColor: Colors.grey[900],
      appBar: AppBar(
        backgroundColor: Colors.blue[900],
        title: Text('Karty: ' + deck.deckname),
        centerTitle: true,
        elevation: 1,
      ),
      body: buildDeckList(),
      floatingActionButton: Padding(
        padding: const EdgeInsets.all(20.0),
        child: FloatingActionButton(
          onPressed: () {
            moveToCardEdit(Flashcard('', '', deck.id), 'Nowa karta');
          },
          child: Icon(Icons.add),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    if (deck.flashcards == null) {
      deck.flashcards = List<Flashcard>();
    }
    updateList();
  }

  ListView buildDeckList() {
    return ListView.builder(
      itemCount: length,
      itemBuilder: (BuildContext context, int index) {
        return Card(
          color: Colors.blueGrey,
          child: ListTile(
            onTap: () {},
            leading: CircleAvatar(
                backgroundColor: Colors.white,
                child: Text((index + 1).toString())),
            title: Text(
              deck.flashcards[index].front,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 24,
                  fontWeight: FontWeight.bold),
            ),
            subtitle: Center(
              child: Text(deck.flashcards[index].back,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontStyle: FontStyle.italic)),
            ),
            trailing: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                ConstrainedBox(
                  constraints: BoxConstraints(minHeight: 35, minWidth: 35),
                  child: GestureDetector(
                    child: Icon(Icons.edit, color: Colors.blue),
                    onTap: () {
                      print('edit');
                      moveToCardEdit(deck.flashcards[index], 'Edytuj karte');
                    },
                  ),
                ),
                ConstrainedBox(
                  constraints: BoxConstraints(minHeight: 35, minWidth: 35),
                  child: GestureDetector(
                    child: Icon(Icons.delete, color: Colors.redAccent),
                    onTap: () {
                      print('delete');
                      _deleteCard(context, deck.flashcards[index]);
                    },
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void moveToCardEdit(Flashcard flashcard, title) async {
    bool result =
        await Navigator.push(context, MaterialPageRoute(builder: (context) {
      return CardDetail(flashcard, title);
    }));

    if (result == true) {
      print(result);
      updateList();
    }
  }

  void _deleteCard(BuildContext context, Flashcard card) async {
    int result = await dbManager.deleteCard(card);
    if (result != 0) {
      _showSnackBar(context, 'Pomyślnie usunięto fiszkę');
      updateList();
    }
  }

  void _showSnackBar(BuildContext context, String message) {
    final snackBar = SnackBar(content: Text(message));
    Scaffold.of(context).showSnackBar(snackBar);
  }

  Future<void> updateList() async {
    List<Flashcard> cards = await dbManager.getCards(this.deck.id);
    setState(() {
      this.deck.flashcards = cards;
      this.length = cards.length;
    });
  }
}
