import 'package:flutter/material.dart';
import 'package:fiszkowo/widgets/menuButton.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[900],
        appBar: AppBar(
          backgroundColor: Colors.blue[900],
          title: Text('Fiszkowo'),
          centerTitle: true,
          elevation: 0,
        ),
        body: Padding(
          padding: EdgeInsets.all(20.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Column(
                children: <Widget>[
                  MenuButton("QUIZ!", 1),
                  MenuButton("NAUKA", 2),
                  MenuButton("KOLEKCJA", 3),
                ],
              ),
            ],
          ),
        ));
  }
}
