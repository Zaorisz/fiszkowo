import 'package:flutter/material.dart';
import 'package:fiszkowo/pages/home.dart';

class ScorePage extends StatelessWidget {
  final int score;
  final int numberOfFlashcards;

  ScorePage(this.score, this.numberOfFlashcards);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.blueAccent,
      child: InkWell(
        onTap: () => Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (BuildContext context) => Home()),
            (Route route) => route == null),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Poprawnych odpowiedzi: ",
              style: TextStyle(
                color: Colors.white,
                fontSize: 30.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              score.toString() + "/" + numberOfFlashcards.toString(),
              style: TextStyle(
                color: Colors.white,
                fontSize: 50.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
