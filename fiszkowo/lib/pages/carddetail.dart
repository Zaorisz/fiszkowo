import 'package:fiszkowo/model/flashcard.dart';
import 'package:flutter/material.dart';
import 'package:fiszkowo/helpers/database_manager.dart';

class CardDetail extends StatefulWidget {
  final String barTitle;
  final Flashcard flashcard;
  CardDetail(this.flashcard, this.barTitle);

  @override
  State<StatefulWidget> createState() {
    return CardDetailState(this.flashcard, this.barTitle);
  }
}

class CardDetailState extends State<CardDetail> {
  String barTitle;
  Flashcard card;
  DatabaseManager dbManager = DatabaseManager();

  TextEditingController frontController = TextEditingController();
  TextEditingController backController = TextEditingController();

  CardDetailState(this.card, this.barTitle);

  @override
  Widget build(BuildContext context) {
    frontController.text = card.front;
    backController.text = card.back;

    return WillPopScope(
      onWillPop: () {
        moveBack();
      },
      child: Scaffold(
        backgroundColor: Colors.grey[900],
        appBar: AppBar(
          backgroundColor: Colors.blue[900],
          title: Text(barTitle),
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                moveBack();
              }),
        ),
        body: Padding(
          padding: EdgeInsets.only(top: 15.0, left: 10.0, right: 10.0),
          child: ListView(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                child: Container(
                  height: 100,
                  child: TextField(
                    maxLines: null,
                    minLines: null,
                    expands: true,
                    maxLength: 50,
                    controller: frontController,
                    style: TextStyle(fontSize: 20, color: Colors.white),
                    onChanged: (value) {
                      debugPrint('Something changed in Front Text Field');
                      updateFront();
                    },
                    decoration: InputDecoration(
                        labelText: 'Przód karty',
                        labelStyle:
                            TextStyle(fontSize: 24, color: Colors.white),
                        filled: true,
                        fillColor: Colors.blueGrey,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0))),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                child: Container(
                  height: 200,
                  child: TextField(
                    maxLines: null,
                    minLines: null,
                    expands: true,
                    maxLength: 255,
                    controller: backController,
                    style: TextStyle(fontSize: 20, color: Colors.white),
                    onChanged: (value) {
                      debugPrint('Something changed in Back Text Field');
                      updateBack();
                    },
                    decoration: InputDecoration(
                        labelText: 'Tył karty',
                        labelStyle:
                            TextStyle(fontSize: 24, color: Colors.white),
                        filled: true,
                        fillColor: Colors.blueGrey,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0))),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: RaisedButton(
                        color: Colors.blue[900],
                        textColor: Colors.white,
                        child: Text(
                          'Zapisz',
                          textScaleFactor: 1.5,
                        ),
                        onPressed: () {
                          setState(() {
                            debugPrint("Save button clicked");
                            _save();
                          });
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void moveBack() {
    Navigator.pop(context, true);
  }

  void updateFront() {
    card.front = frontController.text;
  }

  void updateBack() {
    card.back = backController.text;
  }

  void _save() async {
    moveBack();
    int result;
    if (card.id == null) {
      result = await dbManager.insertCard(card);
    } else {
      result = await dbManager.updateCard(card);
    }
    if (result == 0) {
      // Failure
      _showAlertDialog('Status', 'Wystąpił problem');
    }
  }

  void _showAlertDialog(String title, String message) {
    AlertDialog alertDialog = AlertDialog(
      title: Text(title),
      content: Text(message),
    );
    showDialog(context: context, builder: (_) => alertDialog);
  }
}
